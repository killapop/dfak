---
layout: page
title: Correo electrónico
author: mfc
language: es
summary: métodos de contacto
date: 2018-09
permalink: /es/contact-methods/email.md
parent: /es/
published: true
---

El contenido de tu mensaje así como el hecho de que se contactó a la organización pueden ser accesibles para gobiernos u organismos de seguridad.
